import numpy as np
import matplotlib.pyplot as plt

height = 0
rang = 4*np.pi
t = np.arange(0., rang, 0.1)


def f(t):
    return np.sin(t)


plt.axis([0, rang, -1, 1])
plt.plot(t, f(t), 'b', [0, rang], [height, height])
plt.ylabel('y')
plt.xlabel('x')
plt.show()
