def prime(x):
    if x < 4:
        return True
    for u in range(2, round((x**0.5)+0.51)):
        if x % u == 0:
            return False
    return True


print(prime(2000000))
