# def triangle(n):
#     number = 0
#     for x in range(1, n+1):
#         number = number + x
#     return number
#
#
# print(triangle(8))

def triangle(n):
    x = (n * (n+1))/2
    return x

lst = [0]
counter = 1

while max(lst) < 100000000:
    lst.append(triangle(counter))
    counter += 1

print(len(lst))
