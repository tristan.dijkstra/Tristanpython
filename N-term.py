import matplotlib.pyplot as plt
import numpy as np

lengte = 78
puntVoor55 = 42
# punten = int(input("Hoeveel punten heb je? "))
epsilon = 0.001


def calcCijfer(score, lengte, nterm):
    cijfer = 9 * (score/lengte) + nterm
    return cijfer


def calcCijfer1(score, lengte, nterm, state):
    if state:
        cijfer1 = 1 + score * (9/lengte) * 2
        return cijfer1
    else:
        cijfer1 = 1 + score * (9/lengte) * 0.5
        return cijfer1


def calcCijfer4(score, lengte, nterm, state):
    if state:
        cijfer4 = 10 - (lengte - score) * (9/lengte) * 0.5
        return cijfer4
    else:
        cijfer4 = 10 - (lengte - score) * (9/lengte) * 2
        return cijfer4


def calcNterm(score, lengte, cijfer):
    nterm = cijfer - (9 * (score/lengte))
    return nterm

# Nterm Res
NtR = calcNterm(puntVoor55, lengte, 5.5)
print(NtR)
if NtR > 1:
    state = True
else:
    state = False

# print("uw score is "+ str(calcCijfer(49, 73, 0.9)))
# print("de nterm is "+ str(calcNterm(score, lengte, cijfer)))

plt.axis([0, lengte, 1, 10])
t = np.arange(0, lengte, 1)
forLoopRange = np.arange(0, lengte, 0.001)
# intersections:
snijpunt1 = 0
snijpunt2 = 0
for u in forLoopRange:
    dif = abs(round(calcCijfer(u, lengte, NtR) - calcCijfer1(u, lengte, NtR, state), 2))
    if dif < epsilon:
        print("n1")
        print(u)
        snijpunt1 = u
        break
    else:
        snijpunt = lengte


for u in forLoopRange:
    dif = abs(round(calcCijfer4(u, lengte, NtR, state) - calcCijfer(u, lengte, NtR), 2))
    if dif < epsilon:
        print("n2")
        print(u)
        snijpunt2 = u
        break
    else:
        snijpunt = lengte

# limits
limitCijfer1 = np.arange(0, snijpunt1 + 1, 1)
limitCijferMain = np.arange(snijpunt1, snijpunt2, 1)
limitCijfer2 = np.arange(snijpunt2, lengte + 1, 1)

plt.title("CSE 2018 Wiskunde C")
plt.ylabel("Cijfer")
plt.xlabel("Aantal punten")


if NtR <= 1.1 and NtR >= 0.9:
    plt.plot(t, calcCijfer(t, lengte, NtR))
else:
    plt.plot(limitCijferMain, calcCijfer(limitCijferMain, lengte, NtR))
    plt.plot(limitCijfer1, calcCijfer1(limitCijfer1, lengte, NtR, state))
    plt.plot(limitCijfer2, calcCijfer4(limitCijfer2, lengte, NtR, state))

# plt.plot(t, calcCijfer1(t, lengte, NtR, state))
# plt.plot(t, calcCijfer4(t, lengte, NtR, state))
# plt.plot(t, calcCijfer(t, lengte, NtR))

plt.show()
