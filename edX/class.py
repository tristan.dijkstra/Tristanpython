class Coordinate(object):
    def __init__(self, x,y):
        self.x = x
        self.y = y
    
    def __str__(self):
        return "<" + str(self.x) + ", " + str(self.y) + ">"
    
    def __add__(self, other):
        x_add = self.x + other.x
        y_add = self.y + other.y
        return Coordinate(x_add, y_add)

    def __sub__(self, other):
        x_sub = self.x - other.x
        y_sub = self.y - other.y
        return Coordinate(x_sub, y_sub)

    def distance(self, other):
        x_diff_sq = (self.x - other.x)**2
        y_diff_sq = (self.y - other.y)**2
        return (x_diff_sq + y_diff_sq)**0.5
    
    def


a = Coordinate(3, 4)
o = Coordinate(0, 0)
b = Coordinate(6, 1)

print(a.distance(o))
print(type(a))
print(a - b)
