def fibonacci(x):
    if x < 3:
        return 1
    else:
        return fibonacci(x-1) + fibonacci(x-2)


def prime(x):
    if x < 4:
        return True
    for u in range(2, round((x**0.5)+0.51)):
        if x % u == 0:
            return False
    return True


lst = [1, 2, 3, 4, 5, 6, 7, 8, 9]
test = map(prime, lst)

for u in test:
    print(u)
