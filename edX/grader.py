import math as m


def polysum(n, s):
    """
    Function takes 2 numbers as input and
    outputs the sum of the perimiter squared and the area
    """
    persq = (n * s) ** 2
    area = (0.25 * n * (s**2)) / (m.tan(m.pi/n))
    return round(area + persq, 4)
