def fib(n):
    if n < 3:
        return 1
    else:
        return fib(n-1) + fib(n-2)


dictionary = {}

for u in range(1, 11):
    dictionary[u] = fib(u)

print("fibonacci sequence:")
for u in dictionary:
    print("Nr: ", str(u), " = ", str(dictionary[u]))

print(dictionary)
print(55 in dictionary)
