# square root
x = int(input("type something: " ))
epsilon = 0.01
gues = 0
low = 0.0
high = x
g = (high+low)/2.0

while abs(g**2 - x) >= epsilon:
    gues += 1
    print("low: " + str(low) + " high: " + str(high) + " g: " + str(g) + " guess nr: " + str(gues))
    if g**2 > x:
        high = g
    else:
        low = g
    g = (high+low)/2.0

print("\nawnser: " + str(g))
print("rounded awnser: " + str(round(g)))
print("guesses: " + str(gues))