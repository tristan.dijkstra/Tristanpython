def fibonacci(x):
    """
    recursive fibonacci ting
    input int x. output xth fibonacci number.
    """
    if x < 3:
        return 1
    else:
        return fibonacci(x-1) + fibonacci(x-2)


for u in range(14):
    print(fibonacci(u))
