s = "apple"
lst = ["a", "x", "p", "l", "e", "p"]

def isWordGuessed(secretWord, lettersGuessed):
    '''
    secretWord: string, the word the user is guessing
    lettersGuessed: list, what letters have been guessed so far
    returns: boolean, True if all the letters of secretWord are in lettersGuessed;
      False otherwise
    '''
    lettersGuessedCopy = lettersGuessed[:]
    secretList = (list(secretWord))
    for u in lettersGuessedCopy:
        if u not in secretList:
            lettersGuessedCopy.remove(u)

    print(lettersGuessedCopy)
    print(secretList)
    if sorted(secretList) == sorted(lettersGuessedCopy):
        return True
    else:
        return False


print(isWordGuessed(s, lst))
