num = 24
epsilon = 0.0001
guesses = 0
g = num/2.0

while abs(g**2 - num) >= epsilon:
    guesses += 1
    g = g - ((g**2 - num)/(2*g))

print("awnser: " + str(g))
print("guesses: " + str(guesses))
