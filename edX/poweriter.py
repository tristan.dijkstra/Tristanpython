def iterPower(base, exp):
    '''
    base: int or float.
    exp: int >= 0

    returns: int or float, base^exp
    '''
    result = 1
    for u in range(1, exp+1):
        result *= base
    return result


print(iterPower(5, 0))
