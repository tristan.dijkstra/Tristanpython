import gspread as gsp
from oauth2client.service_account import ServiceAccountCredentials as SAC
import pprint
from time import strftime

scope = ['https://spreadsheets.google.com/feeds', 'https://www.googleapis.com/auth/drive']
creds = SAC.from_json_keyfile_name("client_secret.json", scope)
client = gsp.authorize(creds)
sheet = client.open('PasswordList').sheet1
pp = pprint.PrettyPrinter()


def addPass():
    site = input("What site is the password for: ")
    passw = input("Password for this site: ")
    date = strftime("%Y %m %d")
    line = [site, passw, date]
    sheet.append_row(line)


def searchPass():
    query = input("Search:")
    result = sheet.findall(query)
    rowNum = deformatRow(result)
    print(str(sheet.row_values(rowNum)))


def deformat(w):
    lst = list(str(w))
    count = 0
    for x in lst:
        if x == "'":
            del lst[len(lst)-2:len(lst)]
            del lst[0:count+1]
            break
        count += 1
    final = "".join(lst)
    return final


def deformatRow(w):
    lst = list(str(w))
    return int(lst[8])


def deformatEntry(w):
    for u in range(len(w)):
        w[u] = str(w[u]) + " "
    final = "".join(w)
    return final


def deformatEntryPlus(w):
    for u in range(len(w)):
        w[u] = str(w[u]) + " "
    final = "Site: " + str(w[0]) + "\nPassword: " + str(w[1]) + "\nDate Added: " + str(w[2])
    return final


def listAll():
    length = sheet.row_count
    print("Sites:")
    for u in range(2, length+1):
        print(str(u-1) + " " + deformat(sheet.cell(u, 1)))


print(deformatEntryPlus(sheet.row_values(2)))
listAll()
# print(deformat(sheet.cell(1, 5)))
# searchPass()

# while True:
#     masterKey = input("Enter masterkey or type cancel:")
#     if masterKey == deformat(sheet.cell(1, 5)):
#         for x in range(2):
#             addPass()
#         break
#     elif masterKey == "cancel":
#         break
#     else:
#         print("Masterkey is incorect")
