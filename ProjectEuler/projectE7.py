def prime(x):
    if x < 4:
        return True
    for u in range(2, round((x**0.5))+1):
        if x % u == 0:
            return False
    return True


x = 2
lst = []
while True:
    if prime(x):
        lst.append(x)
    x += 1
    if len(lst) > 10001:
        break

print(len(lst))
print(lst[10000])
