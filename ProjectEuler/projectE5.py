def div(x):
    for m in range(1, 20):
        if x % m != 0:
            return False
    return True


x = 200000000
while True:
    if div(x):
        print(x)
        break
    else:
        x += 1
