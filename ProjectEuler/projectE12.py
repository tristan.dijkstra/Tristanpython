def triangle(n):
    x = (n * (n+1))/2
    return x

lst = [0]
counter = 1

while max(lst) < 100000000:
    x = int(triangle(counter))
    if x % 2:
        lst.append(x)
    counter += 1

print(lst)
print(len(lst))


for u in lst:
    c = 0
    print(u)
    for x in range(1, u+1):
        if u % x == 0:
            c += 1
        if c >= 500:
            print("result: " + str(u))
            break
