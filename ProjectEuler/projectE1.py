def run(n):
    lst = []
    for x in range(0, n):
        five = 5 * x
        three = 3 * x
        if three < n:
            lst.append(three)
        if five < n:
            lst.append(five)
    for x in lst:
        if lst.count(x) > 1:
            lst.remove(x)

    print(lst)
    return sum(lst)


print(run(1000))
