lst = []


def palindrome(x):
    if x == x[::-1]:
        return True


for a in range(100, 1000):
    for b in range(100, 1000):
        c = a * b
        if palindrome(str(c)):
            lst.append(c)

lst.sort(reverse=True)
print(lst[0])
