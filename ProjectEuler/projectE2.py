def fib(n):
    if n < 3:
        return 1
    else:
        return fib(n-1) + fib(n-2)


lst = []
for x in range(1, 34):
    z = fib(x)
    if z > 4000000:
        break
    if z % 2 == 0:
        lst.append(z)

print(str(sum(lst)))
