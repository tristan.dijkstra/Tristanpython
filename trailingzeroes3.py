import math
import numpy as np


def zeroes(n):
    kmax = (math.log(n))/(math.log(5))
    lst = np.arange(1, kmax, 1)
    z = 0
    for x in lst:
        z = z + (n/(5*x))
    return z


print(zeroes(23))
