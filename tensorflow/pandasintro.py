import pandas as pd
pd.__version__

import numpy as np


city_names = pd.Series(['San Francisco', 'San Jose', 'Sacramento'])
population = pd.Series([852469, 1015785, 485199])

cities = pd.DataFrame({'City name': city_names, 'Population': population})

cities['Area square miles'] = pd.Series([46.87, 176.53, 97.92])
cities['Population density'] = cities['Population'] / \
    cities['Area square miles']

cities["Bool"] = (cities['City name'].apply(lambda val: val[0:3] == "San")) & (cities['Area square miles'] > 50)


np.log(population)

print("San" in cities['City name'][2])

print(cities)
