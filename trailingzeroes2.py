import math


def zeroes(n):
    if n < 1:
        return 0
    z = 0
    kmax = (math.log(n))/(math.log(5))
    counter = 1
    while counter < kmax:
        z = z + (n/(5*counter))
        counter += 1
    return round(z)


print(zeroes(27))
